#ifndef _BMP_H_
#define _BMP_H_

#include "image.h"
#include "util_file.h"
#include <stdio.h>

enum read_status from_bmp( FILE* in, struct image* image );

enum write_status to_bmp( FILE* out, struct image const* image );
#endif
