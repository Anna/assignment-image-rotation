#ifndef _CHANGE_H_
#define _CHANGE_H_

enum act{
    ROTATE_COUNTERCLOCKWISE_90
};

struct image change( struct image const image, enum act act );

#endif
