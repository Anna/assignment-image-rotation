#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>
#include <stdio.h>

struct pixel { uint8_t b, g, r; };

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

#endif
