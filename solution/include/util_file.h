#ifndef _UTIL_FILE_H_
#define _UTIL_FILE_H_

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

enum open_status  {
    OPEN_OK,
    OPEN_ERROR
};

/*  deserializer   */
enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
    WRITE_OK,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum close_status  {
    CLOSE_OK,
    CLOSE_ERROR
};

enum file_extension {
    BMP
};

void open_file( FILE ** file, const char * filename, const char * mode );
void read_file( FILE * file, struct image * image, enum file_extension file_extension );
void close_file( FILE * file );
void write_file( FILE * file, const struct image * image, enum file_extension file_extension );

#endif
