#include "../include/bmp.h"
#include "../include/util_file.h"

#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

#define TYPE 0x4D42
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

struct bmp_header create_header( struct image const img ) {
    return (struct bmp_header){
            .bfType = TYPE,
            .bfileSize = (sizeof(struct bmp_header) + img.height* img.width * sizeof(struct pixel)
                          + img.height * ((img.width) % 4)),
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE,
            .biWidth = img.width,
            .biHeight = img.height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = img.height * img.width * sizeof(struct pixel) + (img.width % 4)*img.height,
            .biXPelsPerMeter = X_PELS_PER_METER,
            .biYPelsPerMeter = Y_PELS_PER_METER,
            .biClrUsed = CLR_USED,
            .biClrImportant = CLR_IMPORTANT};
}

static void read_header(FILE *f, struct bmp_header *header) {
    fread( header, sizeof( struct bmp_header ), 1, f );
}

enum read_status from_bmp( FILE* in, struct image* image ){

    struct bmp_header header;

    read_header( in, &header );

    if ( header.bfType != 0x4d42 ) return READ_INVALID_SIGNATURE;
    if ( header.biBitCount != 24 ) return READ_INVALID_BITS;

    image->data = malloc( sizeof( struct pixel ) * header.biWidth * header.biHeight );

    if( header.biWidth && header.biHeight > 0 ) {
        image->width = header.biWidth;
        image->height = header.biHeight;
    } else return READ_INVALID_BITS;

    uint32_t i;

    for( i = 0; i < header.biHeight; i++ ) {
        if( !( fread( &( image->data[i * image->width] ), sizeof( struct pixel ), header.biWidth, in ) ) ) 
            return READ_INVALID_HEADER;
        if( fseek( in, header.biWidth % 4, SEEK_CUR ) )
            return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, const struct image* image ) {
    struct bmp_header head = create_header( *image );

    fwrite( &head, sizeof(struct bmp_header), 1, out );

    const size_t padding = 0;

    const uint32_t width = image->width;
    const uint32_t height = image->height;

    uint32_t i;

    for( i = 0; i < height; i++ ) {
        if( !( fwrite( &( image->data[i * width] ), sizeof( struct pixel ), width, out ) ) ) 
            return WRITE_ERROR;
        if( !( fwrite( &padding, 1, width % 4, out ) ) ) 
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
