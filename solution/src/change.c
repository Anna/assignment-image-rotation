#include "../include/image.h"
#include "../include/change.h"

#include <stdlib.h>

static struct image rotate_counterclockwise_90( const struct image image ){
    struct image output = {0};

    const uint32_t width = image.width;
    const uint32_t height = image.height;

    output.width = height;
    output.height = width;

    output.data = malloc(sizeof(struct pixel) * image.width * image.height);

    for (uint64_t i = 0; i < image.width; i++) {
        for (uint64_t j = 0; j < image.height; j++) {
            output.data[i * height + j] = image.data[(height - 1 - j) * width + i];
        }
    }

    return output;
}

static struct image (*action[])( const struct image image ) = {
    [ROTATE_COUNTERCLOCKWISE_90] = rotate_counterclockwise_90
};

struct image change( const struct image image, enum act act ) {
    return (*action[act])( image );
}
