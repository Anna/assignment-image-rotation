#include "../include/bmp.h"
#include "../include/change.h"
#include "../include/util_file.h"

#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    struct image image= {0};
    struct image result;

    FILE * in;
    open_file( &in, argv[1], "rb");
    FILE *out;
    open_file( &out, argv[2], "wb");

    read_file( in, &image, BMP);
    result = change( image, ROTATE_COUNTERCLOCKWISE_90 );
    write_file( out, &result, BMP );

    close_file( in );
    close_file( out );
    free(image.data);
    free(result.data);

    return 0;
}
