#define _CRT_SECURE_NO_WARNINGS

#include "../include/bmp.h"
#include "../include/util_file.h"

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>

static enum read_status (*file_read[])( FILE * file, struct image * image ) = {
    [BMP] = from_bmp
};
static enum write_status (*file_write[])( FILE * file, const struct image * image ) = {
    [BMP] = to_bmp
};

_Noreturn void error_( const char* err, ... ){
    fprintf(stderr, "%s", err);
    exit(1);
}

void open_file( FILE ** file, const char * filename, const char * mode ){
    *file = fopen( filename, mode );
    enum open_status is_open = OPEN_OK;
    if( !(*file) ) is_open = OPEN_ERROR;

    switch( is_open ){
        case OPEN_ERROR:
            error_( "open error\n" );
            break;
        case OPEN_OK:
            break;
    }
}

void close_file( FILE * file ){
    bool close = fclose( file );
    enum close_status is_close = CLOSE_OK;
    if( close ) is_close = CLOSE_ERROR;

    switch( is_close ){
        case CLOSE_ERROR:
            error_( "close error\n" );
            break;
        case CLOSE_OK:
            break;
    }
}

void read_file( FILE * file, struct image * image, enum file_extension file_extension ){
    enum read_status is_read = (*file_read[file_extension])( file, image );
    
    switch( is_read ){
        case READ_INVALID_SIGNATURE:
            error_("invalid signature\n");
            break;
        case READ_INVALID_BITS:
            error_("invalid bits\n");
            break;
        case READ_INVALID_HEADER:
            error_("invalid header\n");
            break;
        case READ_OK:
            break;
    }
}

void write_file( FILE * file, const struct image * image, enum file_extension file_extension ){
    enum write_status is_write = (*file_write[file_extension])( file, image );
    
    switch ( is_write )
    {
    case WRITE_ERROR:
        error_("write error\n");
        break;
    
    case WRITE_OK:
        break;
    }
}
